def dados_paises () :
    paises_lusofonos = ['Brasil', 'Portugal', 'Angola', 'Macau']
    paises_espanhol = ['Argentina', 'Espanha', 'Uruguai', 'Paraguai']
    paises_ingles = ['EUA', 'Reino Unido', 'Austrália', 'África do Sul']
    for nome in paises_lusofonos:
        print(nome)
        print(type(nome))
    for index, item in enumerate(paises_lusofonos, start=10):
        print(f'{item} tem o índice {index}')
    for paises in zip(paises_lusofonos, paises_espanhol, paises_ingles):
        print(paises) # tuplas
        lista_paises = list(paises)
        lista_paises.append('China')
        print(lista_paises)
        print(paises.count('Brasil'))
        buscar = 'Brasil' in paises # return TRUE or FALSE
        print(buscar)
    for numero in range (0, 11, 2): # começo, fim e intervalo
        print(numero)


def main (): 
    pais = dados_paises()

if __name__ == "__main__" :
    main()
