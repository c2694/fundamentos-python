def func_lambda (x,y): # função já existente no python
    multiplicar = lambda x,y : x*y # exemplo de programação funcional (simplifica)
    return multiplicar(x,y)

def multiplicar (x,y):
    return x*y

def func_map ():
    numeros = [1, 2, 3, 4, 5]
    multiplicar = tuple(map(lambda x: x*2, numeros)) # map >> realiza funções dentro dele >> recebe função e depois uma lista
    # converter em STR com função já existente
    transformar = list(map(str, numeros)) # percorre a lista e converte seus elementos
    print(multiplicar, transformar)
    # converter em STR 
    for index, numero in enumerate(numeros):
        numero = str(numero)
        print(index, numero)
        numeros[index] = numero
    print(numeros)

def main (): 
    exemplo = func_lambda(3,4)
    exemplo2 = multiplicar(7,7)
    ex3 = func_map()
    print(ex3)


if __name__ == "__main__" :
    main()
