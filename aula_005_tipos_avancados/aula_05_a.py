def paises () :
    paises = ['Brasil', 'Portugal', 'Angola', 'Macau'] # elemento iterável 
    cidades = ['Franca', 'Mendoza', 'Rabat', 'Paris']
    """
    print(paises.extend(cidades))
    print(paises.index('Angola'))
    print(cidades) # ok
    paises[1] = 'Macau' # ok
    print(paises)
    """

    # QTD DE CARACTERES [] DO ELEMENTO INDICADO PELO ÍNDICE
    print(len(paises[0]))

    # ADC ELEMENTO 
    print(paises.index('Brasil'))

    # APPEND - ADICIONA ELEMENTOS EM UMA LISTA
    print(paises.append('Guiné Bissau'))

    # DELETAR ELEMENTO DA LISTA PELO ÍNDICE
    del paises[2]
    
    # COPIA A LISTA E QUEBRA A DEPENDÊNCIA
    paises_2 = paises.copy()
    paises_3 = paises # sem o copy
    print(paises.append('Angola'))
    print(f'Os países 2 são: {paises_2}')
    print(f'Os países 3 são: {paises_3}') # sem o copy
    
    # ORDENA
    ordenar = paises.sort()
    print(paises)

    # REVERTE
    reverter = paises.reverse()
    print(paises)

def main (): 
    pais = paises()

if __name__ == "__main__" :
    main()