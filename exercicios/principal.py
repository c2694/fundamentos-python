# TAREFA >> criar uma classe de países 
    # atributos: nome (valor requerido para saber outras informações)
    # métodos (equivalente ao somar): continente, capital e idioma
    # lista de países: Brasil, Argentina, África do Sul, Egito, França, Alemanha, China, Austrália e EUA

class SobrePaises :
    def __init__(self, nome) : # autoreferenciação da classe
        nome = lambda nome : nome.lower()
        self.nome = nome

    def continente(self) :  # colocar em f-string
        if (self.nome == 'brasil') or (self.nome == 'argentina') : # true or false
            return f'O continente de {self.nome.title()} é a América'
        elif self.nome == 'áfrica do sul' : 
            return "África"
        elif self.nome == 'frança' : 
            return "Europa"
        else :
            return "não tem"


    def capital(self) :
        if self.nome == 'brasil' :
            return "Distrito Federal"
        elif self.nome == 'áfrica do sul' :
            return "Cidade do Cabo"
        elif self.nome == 'frança' :
            return "Paris"
        elif self.nome == 'argentina' :
            return "Buenos Aires"
        else :
            return "não tem" 


    def idioma(self) : 
        if self.nome == 'brasil' :
            return "Português - BR"
        elif self.nome == 'áfrica do sul' : 
            return "Inglês"
        elif self.nome == 'frança' :
            return "Francês"
        elif self.nome == 'argentina' :
            return "Espanhol"
        else :
            return "não tem" 

    def temperatura(self) :
        return f'20,04 °C'

    def extensao(self) :
        return f'100 km2'


class Cidade (SobrePaises) :
    def __init__(self, nome_cidade, nome, lista_pais) : # referenciando atributos que irei precisar/passar 
        super().__init__(nome) # atributos da classe mãe >> super() permite herdar, __init__ inicia esta classe mãe
        nome_cidade = nome_cidade.lower()
        self.nome_cidade = nome_cidade
        self.lista_pais = lista_pais


    def qtd_hab (self) :
        if (self.nome_cidade == 'franca') and ((self.nome == '1') or (self.nome == 'brasil')) :
            return (f'{self.nome_cidade} tem 350 mil habitantes\n'
            f'Esta cidade está localizada no país {self.lista_pais[0]}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {super().idioma()}\n' # chamou do super (super vs. sub), na classe mãe (super)
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}') # procura na classe filha (sub), se não houver, pega da classe mãe (polimorfismo)
        elif (self.nome_cidade == 'mendoza') and ((self.nome == '2') or (self.nome == 'argentina')) :
            return (f'{self.nome_cidade} tem 150 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        elif (self.nome_cidade == 'rabat') and ((self.nome == '3') or (self.nome == 'áfrica do sul')) :
            return (f'{self.nome_cidade} tem 577 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        elif (self.nome_cidade == 'paris') and ((self.nome == '5') or (self.nome == 'frança')) :
            return (f'{self.nome_cidade} tem 2,16 milhões de habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        else :
            return f'Essa cidade não faz parte da lista ou a combinação de país e cidade não tem correspondente'
        

    def temperatura (self) : # modificando dados da classe mãe 
        if self.nome_cidade == 'franca' :
            return f'40 °C'
        elif self.nome_cidade == 'mendoza' :
            return f'25 °C'
        elif self.nome_cidade == 'rabat' :
            return f'30 °C'
        elif self.nome_cidade == 'paris' :
            return f'12 °C'

    def extensao (self) :
        if self.nome_cidade == 'franca' :
            return f'632.734 km2'
        elif self.nome_cidade == 'mendoza' :
            return f'54 km2'
        elif self.nome_cidade == 'rabat' :
            return f'117 km2'
        elif self.nome_cidade == 'paris' :
            return f'105.4 km2'

# passar país e capital juntos em zip >> compactar elementos iguais ok
# lista cidade >> unir equivalentes
# printar as tuplas >> deixar em ordem 

def main (): 
    while True :
        lista_pais = ['Brasil', 'Argentina', 'África do Sul', 'França']
        lista_cidade = ['Franca', 'Mendoza', 'Rabat', 'Paris']
        print("Escolha um dos países abaixo")
        for indice, pais in enumerate(lista_pais, start=1):
            print(f'{indice} - {pais}')
            continue
        for locais in zip(lista_pais, lista_cidade):
            print(locais) # tuplas
        nome = input("insira um país: ")
        print(f'Escolha uma cidade desta lista: {lista_cidade}')
        nome_cidade = input("insira uma cidade: ")
        cidades = Cidade(nome_cidade, nome, lista_pais)
        print(cidades.qtd_hab())
        populacao = [{'América': 'Franca', 'pop': 350}, {'África': 'Rabat', 'pop': 577}, {'Europa': 'Paris', 'pop': 2000}]
        # verificação
        print(list(map(lambda x : x['pop'] > 576, populacao)))



if __name__ == "__main__" :
    main()
