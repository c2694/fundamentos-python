class Cidade :
    def __init__(self, nome_cidade) :
        self.nome_cidade = nome_cidade

    def qtd_hab (self) :
        if self.nome_cidade == 'Brasília' :
            return "Brasília tem 3 milhões de habitantes"
        elif self.nome_cidade == 'Buenos Aires' :
            return "Buenos Aires tem 15,5 milhões de habitantes"

def main (): 
    lista_cidades = ['x']
    print(f'Escolha uma das cidades desta lista: {lista_cidades}')
    nome_cidade = input("insira uma cidade: ")
    cidades = Cidade(nome_cidade) # chamando a classe
    print(cidades.qtd_hab())
    # OU
    # habitantes = Cidade("Brasília") >> chamando a classe também

if __name__ == "__main__" :
    main()

