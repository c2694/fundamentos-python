# TAREFA >> criar uma classe de países 
    # atributos: nome (valor requerido para saber outras informações)
    # métodos (equivalente ao somar): continente, capital e idioma
    # lista de países: Brasil, Argentina, África do Sul, Egito, França, Alemanha, China, Austrália e EUA

class SobrePaises :
    def __init__(self, nome) : # autoreferenciação da classe
        nome = nome.lower()
        self.nome = nome

    def continente(self) :  # colocar em f-string
        if (self.nome == 'brasil') or (self.nome == 'argentina') or (self.nome == 'estados unidos') : # true or false
            return f'O continente de {self.nome.title()} é a América'
        elif (self.nome == 'áfrica do sul') or (self.nome == 'egito') : 
            return "África"
        elif (self.nome == 'frança') or (self.nome == 'alemanha') : 
            return "Europa"
        elif self.nome == 'china' :
            return "Ásia"
        elif self.nome == 'austrália' :
            return "Oceania"
        else :
            return "não tem"


    def capital(self) :
        if self.nome == 'brasil' :
            return "Distrito Federal"
        elif self.nome == 'áfrica do sul' :
            return "Cidade do Cabo"
        elif self.nome == 'estados unidos' :
            return "Washington"
        elif self.nome == 'austrália' :
            return "Camberra"
        elif self.nome == 'frança' :
            return "Paris"
        elif self.nome == 'china' :
            return "Pequim"
        elif self.nome == 'argentina' :
            return "Buenos Aires"
        elif self.nome == 'egito' :
            return "Cairo"
        elif self.nome == 'alemanha' :
            return "Berlim"
        else :
            return "não tem" 


    def idioma(self) : 
        if self.nome == 'brasil' :
            return "Português - BR"
        elif (self.nome == 'áfrica do sul') or (self.nome == 'estados unidos') or (self.nome == 'austrália'): 
            return "Inglês"
        elif self.nome == 'frança' :
            return "Francês"
        elif self.nome == 'china' :
            return "Mandarim"
        elif self.nome == 'argentina' :
            return "Espanhol"
        elif self.nome == 'alemanha' :
            return "Alemão"
        elif self.nome == 'egito' :
            return "Árabe"
        else :
            return "não tem" 

    def temperatura(self) :
        return f'20,04 °C'

    def extensao(self) :
        return f'100 km2'


class Cidade (SobrePaises) :
    def __init__(self, nome_cidade, nome) : # referenciando atributos que irei precisar/passar 
        super().__init__(nome) # atributos da classe mãe >> super() permite herdar, __init__ inicia esta classe mãe
        self.nome_cidade = nome_cidade


    def qtd_hab (self) :
        if (self.nome_cidade == 'Franca') and (self.nome == 'Brasil') :
            return (f'{self.nome_cidade} tem 350 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {super().idioma()}\n' # chamou do super (super vs. sub), na classe mãe (super)
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}') # procura na classe filha (sub), se não houver, pega da classe mãe (polimorfismo)
        elif (self.nome_cidade == 'Mendoza') and (self.nome == 'Argentina') :
            return (f'{self.nome_cidade} tem 150 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        elif (self.nome_cidade == 'Rabat') and (self.nome == 'África do Sul') :
            return (f'{self.nome_cidade} tem 577 mil habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        elif (self.nome_cidade == 'Paris') and (self.nome == 'França') :
            return (f'{self.nome_cidade} tem 2,16 milhões de habitantes\n'
            f'Esta cidade está localizada no país {self.nome}\n'
            f'A capital deste país é {self.capital()}\n'
            f'O idioma deste país é {self.idioma()}\n'
            f'A temperatura desta cidade é {self.temperatura()}\n'
            f'A extensão territorial desta cidade é {self.extensao()}')
        else :
            return f'Essa cidade não faz parte da lista ou a combinação de país e cidade não tem correspondente'
        

    def temperatura (self) : # modificando dados da classe mãe 
        if self.nome_cidade == 'Franca' :
            return f'40 °C'
        elif self.nome_cidade == 'Mendoza' :
            return f'25 °C'
        elif self.nome_cidade == 'Rabat' :
            return f'30 °C'
        elif self.nome_cidade == 'Paris' :
            return f'12 °C'

    def extensao (self) :
        if self.nome_cidade == 'Franca' :
            return f'632.734 km2'
        elif self.nome_cidade == 'Mendoza' :
            return f'54 km2'
        elif self.nome_cidade == 'Rabat' :
            return f'117 km2'
        elif self.nome_cidade == 'Paris' :
            return f'105.4 km2'


def main (): 
    lista_pais = ['Brasil', 'Argentina', 'África do Sul', 'Egito', 'França', 'Alemanha', 'China', 'Austrália', 'Estados Unidos']
    lista_cidade = ['Franca', 'Mendoza', 'Rabat', 'Paris']
    print(f'Escolha um dos países desta lista: {lista_pais}')
    nome = input("insira um país: ")
    print(f'Escolha uma cidade desta lista: {lista_cidade}')
    nome_cidade = input("insira uma cidade: ")
    cidades = Cidade(nome_cidade, nome)
    print(cidades.qtd_hab())



if __name__ == "__main__" :
    main()
