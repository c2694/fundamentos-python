"""
funções built-in >> python entrega prontas, funçõoes nativas
print(), type(), input()

funções criadas por nós

As funções podem ter PARÂMETROS e receberem ARGUMENTOS
"""


def nome_funcao(data, hora="00h00", cumprimento="olá"): # entre parenteses >> parametros
    # print("estrutura de função")
    # print(f'{data}, {hora} e {cumprimento}')
    #### pass ou ...
    pass

# nome_funcao("20/10/2021", "10h25", "bom dia") # posicional >> os argumentos seguem a sequência dos parametros
# nome_funcao("bom dia") # completa o parametro sem argumento
# nome_funcao(data="20/10/2021", hora="10h25", cumprimento="bom dia")

### SOMAR

def somar(num_x, num_y): # variáveis ficam isoladas dentro da função
    soma = num_x + num_y
    return print(f'A soma é {soma}') # torna a variável disponível para as outras funções

### SUBTRAIR

def subtrair(num_u, num_w):
    subtracao = num_u - num_w
    print(f'A subtração de {num_u} por {num_w} é igual a {subtracao}')

# MULTIPLICAR

def multiplicar(num_a, num_b):
    multiplicacao = num_a * num_b
    return print(f'A multiplicação de {num_a} por {num_b} é igual a {multiplicacao}')


    # TAREFA >> montar uma f-string para o RETURN
    # TAREFA >> montar uma função de subtração e multiplicação

"""
def funcao2():
    soma = somar(1,1)
    print(soma)

funcao2() # quando a função for chamada, se ela não tiver um return, ela retona NONE
"""

def main() :
    num_u = int(input("insira um número: "))
    num_w = int(input("insira um número: "))
    soma = somar(num_u, num_w)
    subtracao = subtrair(num_u, num_w)
    multiplicacao = multiplicar(num_u, num_w)
    

if __name__ == "__main__" :
    main()