from aula_03_classes import CalculadoraSimples # importando a classe 


def main ():
    valores = CalculadoraSimples(3,4) # instanciando a classe
    valores2 = CalculadoraSimples(1,2) # outra
    valores3 = CalculadoraSimples(1,1)
    print(valores.somar(valores2))
    print(valores.subtrair(valores3))
    print(valores.multiplicar(valores))
    # print(valores.subtrair()) # chamando a função apontada na main do outro arquivo (importada)
    # neg = valores.negativo()
    # print(neg.num_x)
    # print(valores.negativo()) # retorna a localização na máquina
    print(valores.somar())
    print(dir(valores)) # retorna funções disponíveis para utilizar (tanto do python, quanto criadas pelo usuário) 


if __name__ == "__main__" :
    main()