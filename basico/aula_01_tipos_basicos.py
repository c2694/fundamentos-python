# Comentários

"""
Docstrings
    - documentar funções e classes
    - comentar um bloco de codigo
números (int, float, complex)
string (str): lista de caracteres
    índices: cada caractere é um índice (inclusive espaço)
"""

### Funções Built-in

# print ("Olá Mundo!")
# print(type("Olá Mundo!"))
### input()
### lista = []

### Funções criadas pelos funcionários
def nome_funcao():
    """Docstring"""
    pass

#### Variáveis
cumprimento = "Olá mundo" # tipo de dados básico chamado de string >> str
# print(cumprimento, type(cumprimento))
cumprimento2 = f'Esta é uma f-string que contém a variável cumprimento: {cumprimento}' # f-string
# print(cumprimento2)
cumprimento3 = (3, "CÍNTIA")
# print(cumprimento3)
nome, data, cor = "Cíntia", "20/10/2021", "azul"
# print(nome, data, cor)
cumprimento4 = f'Os dados são... nome: {nome}, data: {data} e cor: {cor}'
# print(cumprimento4)

####  NÚMEROS E CONVERSÕES
numero_ex01 = 3 # tipo de dados básico chamado de numeros inteiros >> int
# print(numero_ex01, type(numero_ex01)) 
numero_ex02 = int("4")
# print(numero_ex02, type(numero_ex02)) 

numero_x = "5" # str
numero_y = 5.5 # FLOAT (número decimal)
numero_z = 10 # int
# numero_x = int(input("insira um número: ")) # inserir
# numero_y = int(input("insira um número: "))
# numero_z = int(input("insira um número: "))
somar = float(numero_x) + numero_y + numero_z
subtrair = int(numero_x) - numero_y - numero_z
multiplicar = int(numero_x) * numero_y * numero_z
# print(somar, type(somar)) # TAREFA: Fazer com -f string
somar_xyz = f'A soma de X ({numero_x}), Y ({numero_y}) e Z ({numero_z}) é igual a {somar}' 
# print(somar_xyz)
# print(subtrair, type(subtrair))
subtrair_xyz = f'A subtração de X ({numero_x}), Y ({numero_y}) e Z ({numero_z}) é igual a {subtrair}' 
# print(subtrair_xyz)
# print(multiplicar, type(multiplicar))
multiplicar_xyz = f'A multiplicação de X ({numero_x}), Y ({numero_y}) e Z ({numero_z}) é igual a {multiplicar}' 
# print(multiplicar_xyz)

### STR
### métodos de str - funções dentro de uma classe

lista_caracteres = " uma frase "
# print(lista_caracteres[0:3]) # do índice 0 à 3
# print(lista_caracteres[:3]) # até índice 3
# print(lista_caracteres[-1]) # último índice
# print(lista_caracteres[-5:]) # a partir do índice -5 (trás pra frente)
# print(lista_caracteres[::]) # pega do primeiro ao último índice
# print(lista_caracteres[::3]) # pega o primeiro índice e o índice indicado (pula os elementos)

substrings = lista_caracteres.split() # iterável - podemos percorrer elementos
print(substrings)
for elemento in substrings: # interação
    print(elemento)
    print(f'O elemento dessa lista é "{elemento}" e seu índice é "{substrings.index(elemento)}"')

print(lista_caracteres.strip()) # retira os espaços do começo e fim
print(lista_caracteres.upper().strip()) # deixa em caixa alta e retira espaços do começo e fim
print(lista_caracteres.lower()) 
print(lista_caracteres.title()) 
print(lista_caracteres.swapcase()) # inverte caixa alta e lowcase
print(lista_caracteres.replace("uma", "1")) 
print(lista_caracteres.count("a")) # quantas dessa STR tem na lista


######
## operações com string
## + ou *
######

#### operadores str
# print(nome + " " + data + " " + cor) # jeito mais antigo >> melhor utilizar -f string
# print(nome * 3)



