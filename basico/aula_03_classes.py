class CalculadoraSimples :
    def __init__(self, num_x, num_y): # responsavel por iniciar a classe e definir seus atributos
        # SELF >> classe se autoreferencia (pode ser qualquer palavra)
        self.num_x = num_x # num_x/y apontando atributos
        self.num_y = num_y 

    # outra = 0 >> torna-se um valor default, entrando no lugar se estiver vazio 
    def somar(self, outra=0) : # método >> é uma função específica dentro de uma classe, e excecuta a acao apontada 
        if outra : # se for verdadeiro que tenha uma outra (não vazia)
            return f'{self.num_x + self.num_y + outra.num_x + outra.num_y}'
        return f'A soma de {self.num_x} com {self.num_y} é igual a {self.num_x + self.num_y}' 

    def subtrair(self, outra=0) :  
        if outra : 
            return f'{self.num_x - self.num_y - outra.num_x - outra.num_y}'
        return f'A subtração de {self.num_x} por {self.num_y} é igual a {self.num_x - self.num_y}'

    def multiplicar(self, outra=0) :
        if outra : 
            return f'{self.num_x * self.num_y * outra.num_x * outra.num_y}'
        return f'A multiplicação de {self.num_x} por {self.num_y} é igual a {self.num_x * self.num_y}'

    def negativo(self) :
        return CalculadoraSimples(- self.num_x , self.num_y) 

    # customizando uma função já existente no python 
    def __str__(self) : # substitui a localização na memoria ram ppelo o que vai ser apontado
        representar = f'{- self.num_x}, {self.num_y}' # apontando o que queremos ver
        return representar

def main (): # só é executada quando eu chamar este script no arquivo que estou rodando
    calculadora = CalculadoraSimples(1,2) # chamando a Classe
    # print(calculadora.somar()) # chamando o método 'somar' na calculadora
    # print(calculadora.subtrair())
    # print(calculadora.multiplicar())

if __name__ == "__main__" :
    main()
