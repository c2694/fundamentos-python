# classe >> um molde

# print(type("str"))
# print(dir(str))

# __init__ >> responsável por iniciar a classe
# (atributos)

class CalculadoraSimples : 
    
    def __init__ (self, x, y) : 
        self.num_x = x
        self.num_y = y
    
    def somar (self) :
        soma = self.num_x + self.num_y
        return soma
