def condicionais ():
    num_1 = 3 
    num_2 = 3
    print(num_1 == num_2) 
    print(num_1 is num_2)
    x = [1,2,3]
    y = [1,2,3]
    # x e y são iguais, mas não idênticos - locais diferentes na memória ram
    print(x == y) 
    print(x is y) 
    if (3 < 5) == True : # operador de igualdade
        print("Não faça isso")
    if (3 < 5) is True : # operador de identidade (compartilha o mesmo lugar na memória)
        print("Não faça isso")
    if (3 < 5) : # por padrão, é TRUE
        print("Faça isso")
    A = [] # false
    B = [1,2]
    C = [3,4]
    # print(bool(A))
    if not A :
        # return print(f'If {A}') # executa o return e finaliza as operações
        print(f'If {A}')  
    elif not B :
        print(f'Elif {B}')
    elif C :
        print(f'Elif {C}')
    else :
        print(f'Else {A}')
    print("fim das condições")

def controle_break():
    paises = ['Brasil', 'Portugal', 'Cabo Verde', 'Macau']
    for pais in paises:
        print(paises[- 1])
        print(paises[-2])
        if pais == 'Brasil':
            print("Capital Brasília")
            # continue # precisa estar vinculado a um loop de repetição / vai para o próximo elemento da iteração 
            # return # finaliza após o return e para as demais operações da função
            # break # para aquele bloco e sai, continuando porém na função
        elif pais == 'Portugal':
            print("Capital Lisboa")
        if pais == 'Brasil':
            print("Capital Brasília")
    print("Fim")


def main (): 
    # exemplo_1 = condicionais()
    exemplo_2 = controle_break()

if __name__ == "__main__" :
    main()